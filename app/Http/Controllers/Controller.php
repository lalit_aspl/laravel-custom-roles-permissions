<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {
        return view('welcome');
    }

    // To Give the Role Permission
    public function updateRolePermissions($request)
    {
        // Find the role
        $role = Role::findOrFail($request->role_id);    
        // Get permission IDs based on permission names
        $permissionNames = $request->users_permission ?? [];
        $permissions = PermissionModel::whereIn('name', $permissionNames)->get();
        $permissionIds = $permissions->pluck('id')->toArray();    
        // Sync permissions with role_permissions table
        $role->permissions()->sync($permissionIds);
    
        return response()->json(['message' => 'Permissions updated for role with ID ' . $role->id]);
    }


   // update User Permission

    public function updatePermissions(Request $request, $userId)
    {
        $user = User::findOrFail($userId); // Assuming you're passing user ID in the URL or request data

        // Your logic to update permissions here
        $user->givePermission($request->users_permission); // Assuming $request->users_permission contains an array of permission names

        return response()->json(['message' => 'Permissions updated successfully']);
    }
}
