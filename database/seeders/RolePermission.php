<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
class RolePermission extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list =[
            [
                'role_id' => '1',                
                'permission_id' => "1",               

            ],
            [
                'role_id' => '1',                
                'permission_id' => "2",             

            ],
            [
                'role_id' => '1',                
                'permission_id' => "3",               

            ],
            [
                'role_id' => '1',                
                'permission_id' => "4",               

            ],
            [
                'role_id' => '1',                
                'permission_id' => "5",          

            ],

            [
                'role_id' => '2',                
                'permission_id' => "1",          

            ],
            [
                'role_id' => '2',                
                'permission_id' => "2",          

            ],
            [
                'role_id' => '2',                
                'permission_id' => "3",          

            ],

        ];

        DB::table('role_permissions')->truncate();
        DB::table('role_permissions')->insert($list);
    }
}
