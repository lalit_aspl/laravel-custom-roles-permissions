<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list =[
            [
                'name' => 'view',                
                'created_at' => date("Y-m-d H:i:s"),               

            ],
            [
                'name' => 'create',                
                'created_at' => date("Y-m-d H:i:s"),               

            ],
            [
                'name' => 'edit',               
                'created_at' => date("Y-m-d H:i:s"),               

            ],
            [
                'name' => 'update',               
                'created_at' => date("Y-m-d H:i:s"),               

            ],
            [
                'name' => 'delete',               
                'created_at' => date("Y-m-d H:i:s"),               

            ],
         

        ];

        DB::table('permissions')->truncate();
        DB::table('permissions')->insert($list);
    }
}
