<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
class Roles extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list =[
            [
                'name' => 'admin',               
                

            ],
            [
                'name' => 'user',               
                

            ],
            
           
         

        ];

        DB::table('roles')->truncate();
        DB::table('roles')->insert($list);
    }
}
