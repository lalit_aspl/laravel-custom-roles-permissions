<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
class UserRole extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list =[
            [
                'user_id' => '1',                
                'role_id' => "1",               

            ],
            
           
         

        ];

        DB::table('user_roles')->truncate();
        DB::table('user_roles')->insert($list);
    }
}
